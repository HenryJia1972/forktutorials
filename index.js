'use strict';

const https = require('https');
const querystring = require('querystring');
const fs = require('fs');
const path = require('path');

const app = require('express')();
const hostname = '8080-ee54db6c-4cc0-4afa-bdfc-6d9fedd1cbc7.ws-us02.gitpod.io';
const port = 8080;

//const AUTH_REDIRECT_URI = `https://${hostname}:${port}/auth`;

const AUTH_REDIRECT_URI = 'https://8080-ee54db6c-4cc0-4afa-bdfc-6d9fedd1cbc7.ws-us02.gitpod.io/auth';

const ProviderType = {
    Google: 'g',
    Dropbox: 'd',
    OneDrive: 'o'
};

const RegisteredApps = {
    google: {
        id: '733546402456-2pc0gmpl0u35ugg7poloa5dnqi4jvt65.apps.googleusercontent.com',
        key: 'Kez3ij0Eu74ZhlWiBaFl6ei9'
    },
    microsoft: {
        id: '4748abad-d64f-4d4f-ae6c-8d197bfc1a2d',
        key: 'Bs__QWRZz4RyvML893-To2r7B.zg6v~qtC'
    },
    dropbox: {
        id: '1vcrqlocluhnx7q',
        key: 'pv5uhrmbeupdy3u'
    }
};

function setAllowOrigin(res) {
    res.set('Access-Control-Allow-Origin', '*');
}

//#region Study - Testing code
app.get('/test', (req, res) => {
    setAllowOrigin(res);
    res.write("get data: " + (req.query.name || ''));
    res.end();
});

app.get('/hello', (req, res) => {
    setAllowOrigin(res);
    res.write("Welcome!");
    res.end();
});

app.get('/hi', (req, res) => {
    res.writeHead(302, { 'Location': 'http://localhost:3000/' });
    res.end();
});
//#endregion

function getPromise(url, options, postData) {
    return new Promise((resolve, reject) => {
        const request = https.request(url, options, (res) => {
            if (res.statusCode === 302 || res.statusCode === 301) {
                getPromise(res.headers.location, options).then(resolve).catch(reject);
                return;
            }
            let data = [];
            res.on('data', (chunk) => {
                data.push(chunk);
            });
            res.on('end', () => {
                try {
                    resolve(Buffer.concat(data));
                } catch (e) {
                    console.error(e.message);
                }
            });
            res.on('error', (error) => {
                reject(error);
            });
        });

        request.on('error', (e) => {
            console.error(`problem with request: ${e.message}`);
        });

        if (postData) {
            request.write(postData);
        }
        request.end();
    });
}

async function makeSynchronousRequest(url, options, postData) {
    try {
        let http_promise = getPromise(url, options, postData);
        let buffer = await http_promise;

        // holds response from server that is passed when Promise is resolved
        return buffer;
    }
    catch (error) {
        // Promise rejected
        console.log(error);
    }
}

function getCacheKey(appId, type) {
    return [appId, type].join('_');
}

app.get('/auth', (req, res) => {
    var { code: authCode, state } = req.query;
    const [appId, type] = state.split('_');

    function getDropboxExchangeCodeContext() {
        var url = 'https://api.dropboxapi.com/oauth2/token';

        var parameters = {
            'code': authCode,
            'grant_type': "authorization_code",
            'client_id': RegisteredApps.dropbox.id,
            'client_secret': RegisteredApps.dropbox.key,
            'redirect_uri': AUTH_REDIRECT_URI
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return { url, options, postData };
    }

    function getGoogleExchangeCodeContext() {
        var url = 'https://oauth2.googleapis.com/token';

        var parameters = {
            'code': authCode,
            'grant_type': "authorization_code",
            'client_id': RegisteredApps.google.id,
            'client_secret': RegisteredApps.google.key,
            'redirect_uri': AUTH_REDIRECT_URI
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return { url, options, postData };
    }

    function getMicrosoftExchangeCodeContext() {
        var url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';

        var parameters = {
            'client_id': RegisteredApps.microsoft.id,
            'scope': 'https://graph.microsoft.com/Files.Read',
            'code': authCode,
            'grant_type': "authorization_code",
            'client_secret': RegisteredApps.microsoft.key,  // NOTE: Only required for web apps. This secret needs to be URL-Encoded.
            'redirect_uri': AUTH_REDIRECT_URI
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return { url, options, postData };
    }

    var context;

    switch(type) {
        case ProviderType.Dropbox:
            context = getDropboxExchangeCodeContext();
            break;
        case ProviderType.Google:
            context = getGoogleExchangeCodeContext();
            break;
        case ProviderType.OneDrive:
            context = getMicrosoftExchangeCodeContext();
            break;
        default:
            res.writeHead(302, { 'Location': `https://krew.kintone.com/k/${appId}/` });
            res.end();
            return;
    }

    (async function () {
        try {
            var { url, options, postData } = context;
            const buffer = await makeSynchronousRequest(url, options, postData);
            const parsedData = JSON.parse(buffer.toString());
            console.log(parsedData);
            saveAuthInfo(getCacheKey(appId, type), parsedData);

            res.writeHead(302, { 'Location': `https://krew.kintone.com/k/${appId}/` });
            res.end();
        } catch (e) {
            console.error(e.message);
        }
    })();
});

function saveAuthInfo(key, tokenInfo, update) {
    if (tokenInfo && tokenInfo.access_token) {
        var authInfo = __cache[key];
        if (!authInfo) {
            __cache[key] = authInfo = {};
        }
        authInfo.t = Date.now() + ((tokenInfo.expires_in || 1800) - 30) * 1000;
        authInfo.a = tokenInfo.access_token;
        if (!update) {
            authInfo.r = tokenInfo.refresh_token;
        }
        saveToFile(key, authInfo);
    } else {
        console.log('Save with wrong result', tokenInfo);
    }
}

function saveToFile(key, authInfo) {
    let filePath = path.join(__dirname, key) + '.json';
    fs.writeFile(filePath, JSON.stringify(authInfo, null, 2), (err) => {
        if (err) {
            console.log('Save file with error:', err);
            return;
        }
        console.log('The file has been saved!', filePath);
    });
}

function getDropboxAuthUrl(appId) {
    const oauth2Endpoint = 'https://www.dropbox.com/oauth2/authorize';

    const parameters = {
        'token_access_type': 'offline',
        'response_type': 'code',
        'state': appId + '_' + ProviderType.Dropbox,
        'redirect_uri': AUTH_REDIRECT_URI,
        'client_id': RegisteredApps.dropbox.id
    };

    return oauth2Endpoint + '?' + querystring.stringify(parameters);
}

function getGoogleAuthUrl(appId) {
    const oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth';

    const parameters = {
        'scope': 'https://www.googleapis.com/auth/drive.metadata.readonly https://www.googleapis.com/auth/drive.readonly',
        'access_type': 'offline',
        'include_granted_scopes': 'true',
        'response_type': 'code',
        'state': appId + '_' + ProviderType.Google,
        'redirect_uri': AUTH_REDIRECT_URI,
        'client_id': RegisteredApps.google.id
    };

    return oauth2Endpoint + '?' + querystring.stringify(parameters);
}

function getMicrosoftAuthUrl(appId) {
    const oauth2Endpoint = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize';

    const parameters = {
        'client_id': RegisteredApps.microsoft.id,
        'scope': 'offline_access https://graph.microsoft.com/Files.Read',
        'response_type': 'code',
        'state': appId  + '_' + ProviderType.OneDrive,
        'redirect_uri': AUTH_REDIRECT_URI,
        'response_mode': 'query',
        'prompt': 'consent'
    };

    return oauth2Endpoint + '?' + querystring.stringify(parameters);
}

app.get('/authUrl', (req, res) => {
    const { appId, type } = req.query;
    setAllowOrigin(res);
    switch (type) {
        case ProviderType.Dropbox:
            res.end(getDropboxAuthUrl(appId));
            return; 
        case ProviderType.Google:
            res.end(getGoogleAuthUrl(appId));
            return;
        case ProviderType.OneDrive:
            res.end(getMicrosoftAuthUrl(appId));
            return;
        default:
            res.end(`Type ${type} not supported yet!`);
            return;
    }
});

var __cache = {};
async function getInfo(appId, type) {
    const key = getCacheKey(appId, type);
    var authInfo = __cache[key];
    if (!authInfo) {
        let filePath = path.join(__dirname, key) + '.json';
        if (fs.existsSync(filePath)) {
            __cache[key] = authInfo = require(filePath);
        }
    }
    if (authInfo) {
        if (authInfo.t < Date.now()) {
            // need get new token
            const done = await doRefreshToken(key, authInfo, type);
            if (done) {
                return authInfo;
            }
        } else {
            return authInfo;
        }
    }
}

async function doRefreshToken(key, authInfo, type) {
    function getDropboxRefreshTokenContext(refresh_token) {
        const url = 'https://api.dropboxapi.com/oauth2/token';
        const parameters = {
            'client_id': RegisteredApps.dropbox.id,
            'client_secret': RegisteredApps.dropbox.key,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return { url, options, postData };
    }

    function getGoogleRefreshTokenContext(refresh_token) {
        const url = 'https://oauth2.googleapis.com/token';
        const parameters = {
            'client_id': RegisteredApps.google.id,
            'client_secret': RegisteredApps.google.key,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return { url, options, postData };
    }

    function getMicrosoftRefreshTokenContext(refresh_token) {
        const url = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';
        const parameters = {
            'client_id': RegisteredApps.microsoft.id,
            'scope': 'https://graph.microsoft.com/Files.Read',
            'client_secret': RegisteredApps.microsoft.key,
            'grant_type': 'refresh_token',
            'refresh_token': refresh_token
        };

        var postData = querystring.stringify(parameters);

        var options = {
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            }
        };

        return {url, options, postData };
    }

    async function getNewAccessToken(context) {
        try {
            const  { url, options, postData } = context;
            const buffer = await makeSynchronousRequest(url, options, postData);
            const parsedData = JSON.parse(buffer.toString());
            return parsedData;
        } catch (e) {
            console.error(e.message);
        }
    }

    const token = authInfo.r;
    let requestContext;
    switch(type) {
        case ProviderType.Dropbox:
             requestContext = getDropboxRefreshTokenContext(token);
            break;
        case ProviderType.Google:
             requestContext = getGoogleRefreshTokenContext(token);
            break;
        case ProviderType.OneDrive:
            requestContext = getMicrosoftRefreshTokenContext(token);
            break;
        default:
            return;
    }

    const tokenInfo = await getNewAccessToken(requestContext);
    if (tokenInfo) {
        saveAuthInfo(key, tokenInfo, true);
        return true;
    }
}

async function getGoogleFiles(token) {
    var url = 'https://www.googleapis.com/drive/v2/files?key=' + RegisteredApps.google.key;
    var options = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    };
    try {
        const buffer = await makeSynchronousRequest(url, options);
        const parsedData = JSON.parse(buffer.toString());
        return parsedData;
    } catch (e) {
        console.error(e.message);
    }
}

async function getOneDriveFiles(token, pathId) {
    var isRoot = !pathId;
    var url = "https://graph.microsoft.com/v1.0/me/drive/" + (isRoot ? "root" : "items/" + pathId) + "/children";
    var options = {
        headers: {
            'Authorization': 'Bearer ' + token
        }
    };
    try {
        const buffer = await makeSynchronousRequest(url, options);
        const parsedData = JSON.parse(buffer.toString());
        return parsedData;
    } catch (e) {
        console.error(e.message);
    }
}

async function getDropboxFiles(token, pathId) {
    var url = "https://api.dropboxapi.com/2/files/list_folder";
    var postData = JSON.stringify({ path: pathId || '' });
    var options = {
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    };

    try {
        const buffer = await makeSynchronousRequest(url, options, postData);
        const parsedData = JSON.parse(buffer.toString());
        return parsedData;
    } catch (e) {
        console.error(e.message);
    }
}

async function downloadFile(url, options) {
    try {
        const buffer = await makeSynchronousRequest(url, options);
        return buffer;
    } catch (e) {
        console.error(e.message);
    }
}

app.get('/list', (req, res) => {
    const { appId, type, pathId } = req.query;
    setAllowOrigin(res);

    getInfo(appId, type).then((info) => {
        if (info) {
            if (type === ProviderType.Google) {
                (async function () {
                    const data = await getGoogleFiles(info.a);
                    res.json(data);
                })();
            } else if (type === ProviderType.OneDrive) {
                (async function () {
                    const data = await getOneDriveFiles(info.a, pathId);
                    res.json(data);
                })();
            } else if (type === ProviderType.Dropbox) {
                (async function () {
                    const data = await getDropboxFiles(info.a, pathId);
                    res.json(data);
                })();
            }
        } else {
            res.writeHead(401);
            res.end();
        }
    });
});

app.get('/download', (req, res) => {
    const { appId, type, id } = req.query;
    setAllowOrigin(res);

    getInfo(appId, type).then((info) => {
        if (info) {
            (async function () {
                var url;
                var options = {
                    headers: {
                        'Authorization': 'Bearer ' + info.a
                    }
                };
                switch(type) {
                    case ProviderType.Dropbox:
                        url = 'https://content.dropboxapi.com/2/files/download';
                        options.method = 'POST';
                        options.headers['Dropbox-API-Arg'] = JSON.stringify({ path: id });
                        break;
                    case ProviderType.Google:
                        const mimeType = req.query.mimeType;
                        url = 'https://www.googleapis.com/drive/v2/files/' + id;
                        if (mimeType) {
                            url += '/export?mimeType=' + mimeType;
                        } else {
                            url += '?alt=media';
                        }
                        break;
                    case ProviderType.OneDrive:
                        url = "https://graph.microsoft.com/v1.0/me/drive/items/" + id + "/content";
                        break;
                    default:
                        res.sendStatus(404);
                        return;
                }
                const buffer = await downloadFile(url, options);
                res.send(buffer);
            })();
        } else {
            res.writeHead(401);
            res.end();
        }
    });
});

app.get('/async', (req, res) => {
    console.log('111');
    (async function () {
        console.log('333');
        var result = await makeSynchronousRequest('https://www.usefulangle.com/api?api_key=123456', {});
        //console.log(result);
        res.write(result);
        res.end();
        console.log('444');
    })();
    console.log('222');

});

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.listen(port);
